# Advent of code 2018
My repo for the 2018 solutions of advent of code 2017.

I used the helper cargo_aoc on this project. And now is required in order to compile
this.

## Instructions 
You will need to install cargo-aoc manually and through cargo, I can't add it as a dependency to the project since it's a cargo plugin and has to be installed separately.
```bash
cargo install cargo-aoc
```

You will need to install cargo-aoc manually through cargo, I can't add it as a project dependency since it's a cargo plugin and can only be installed separately.

**Beware, `cargo-aoc` will need to be configured with your own unique session token**, please [see here](https://github.com/gobanos/cargo-aoc) for more information. But after it has been configured, you may use `cargo aoc input -d <day>` subcommand to download the inputs of your own account into the inputs folder automatically which cargo-aoc will need in order to make the standalone binary. 

If you don't want to or can't download the inputs you may delete the `main.rs` file and run it directly through `cargo aoc -d <day>`, which will make a http request on the fly and pass it through my solutions.

