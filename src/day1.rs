use std::collections::HashSet;

#[aoc(day1, part1)]
pub fn part1(input: &str) -> i64 {
    let mut ttl: i64 = 0;
    for line in input.trim().split("\n") {
        let numb: i32 = line.parse::<i32>().unwrap();
        ttl += numb as i64;
    }
    ttl
}

#[aoc(day1, part2)]
pub fn part2(input: &str) -> i64 {
    let mut previous_val = HashSet::<i64>::new();
    let mut ttl: i64 = 0;
    let lines: Vec<i64> = input
        .trim()
        .split("\n")
        .map(|x| x.parse::<i64>().expect(format!("{}", x).as_str()))
        .collect();
    let mut i = 0;
    loop {
        let line = lines[i % lines.len()];
        i += 1;
        ttl += line;
        if !previous_val.insert(ttl) {
            return ttl;
        }
    }
}
